const express = require("express");
const router = express.Router();
const productController = require("../controllers/productControllers");
const auth = require("../auth");


// Route for creating a product (ADMIN only)
router.post("/createProduct", auth.verify, (req,res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	
	productController.createProduct(data).then(resultFromController => res.send(resultFromController));
})


// Route for retrieving all active products
router.get("/active", (req,res) => {
	productController.getActiveProducts().then(resultFromController => res.send(resultFromController))
})


// Route for retrieving all products
router.get("/", (req,res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController))
})


// Route for retrieving single product
router.get("/:productId", (req,res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController))
})

// Route for updating product information (ADMIN ONLY)
router.put("/:productId",auth.verify,(req,res)=>{
	productController.updateProduct(req.params,req.body).then(resultFromController=>res.send(resultFromController));
});


// Route for archiving a product
router.put("/:productId/archive", auth.verify,(req,res) => {
	productController.archiveProduct(req.params).then(resultFromController => res.send(resultFromController))
})

// Router for activating a Product
router.put("/:productId/activate", auth.verify,(req,res) => {
	productController.activateProduct(req.params).then(resultFromController => res.send(resultFromController))
})

module.exports = router;