const express = require("express");
const router = express.Router();

const userController = require("../controllers/userControllers");
const auth = require("../auth")

//Route for user registration
router.post("/register",(req,res)=>{
	userController.registerUser(req.body).then(resultFromController=>res.send(resultFromController));
})


// Route to check if email exists
router.post("/checkEmail",(req,res)=>{
userController.checkEmailExists(req.body).then(resultFromController=>res.send(resultFromController));
})


//Route for user authentication
router.post("/login",(req,res)=>{
	userController.loginUser(req.body).then(resultFromController=>res.send(resultFromController));
});


// Route for retrieving user details
router.get("/details", auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization)

	userController.getProfile({userId:userData.id}).then(resultFromController => res.send(resultFromController))
})




router.post("/checkout", auth.verify, (req, res)=> {

        let data = {
            isAdmin: auth.decode(req.headers.authorization).isAdmin,
            userId: auth.decode(req.headers.authorization).id,
            totalAmount: req.body.totalAmount,
            productId: req.body.productId,
            quantity: req.body.quantity
        
        };
        userController.createOrder(data).then(resultFromController=> res.send(resultFromController));
});





///////////////////////////// STRETCH GOALS//////////////////////

// Router for making user an admin
router.put("/:userId/setAsAdmin", auth.verify,(req,res) => {
	userController.makeAdmin(req.params).then(resultFromController => res.send(resultFromController))
})

module.exports = router;