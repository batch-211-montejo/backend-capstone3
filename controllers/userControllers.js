const User = require("../models/User");
const Product = require("../models/Product")
const bcrypt = require("bcrypt");
const auth = require("../auth");



// User Registration

module.exports.registerUser = (reqBody) =>{
    let newUser = new User({
        firstName : reqBody.firstName,
        lastName : reqBody.lastName,
        email : reqBody.email,
        mobileNo : reqBody.mobileNo,
       
        password : bcrypt.hashSync(reqBody.password,10)
    })
   
    return newUser.save().then((user,error)=>{
     
        if(error){
            return false;
       
        }else{
            return true;
        };
    });
};


// Check Email
module.exports.checkEmailExists = (reqBody) =>{
	return User.find({email:reqBody.email}).then(result=>{
		if(result.length>0){
			return true;
		}else{
			return false;
		};
	});
};




// User Authentication
module.exports.loginUser = (reqBody) =>{
	return User.findOne({email:reqBody.email}).then(result=>{
		if(result==null){
			return false;
		}else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password,result.password);
			if(isPasswordCorrect){
				return { access: auth.createAccessToken(result)}
			}else{
				return false
			};
		};
	});
};


// Get User Deatils
module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result => {
		return result;
	})
}



// Create Order
module.exports.createOrder = async (data)=> {
	if(data.isAdmin == false) {
    let isUserUpdated = await User.findById(data.userId).then(user=> {
        user.orders.push({
        	totalAmount: data.totalAmount,
        	products: [{
        		productId: data.productId,
        		quantity: data.quantity
        	}]
        });
        return user.save().then((user, error)=> {
            if(error) {
                return false;
            } else {
                return true;
            };
        });
    });

    let isProductUpdated = await Product.findById(data.productId).then(product=> {
        product.orders.push({quantity: data.quantity});
        return product.save().then((product, error)=> {
            if(error) {
                return false;
            } else {
                return true; 
            }
        });
    });

    if(isUserUpdated && isProductUpdated) {
        return true;
    } else {
        return false;
    };
} else {
	return "Admin is not allowed to create order"
}

    
};






///////////////////////////// STRETCH GOALS//////////////////////

// Make User Admin
module.exports.makeAdmin = (reqParams) => {

	let updateUserRole = {
		isAdmin : true
	};

	return User.findByIdAndUpdate(reqParams.userId, updateUserRole).then((user,error) => {

		if (error) {

			return false;

		} else {

			return user;

		}

	});
};                                                    