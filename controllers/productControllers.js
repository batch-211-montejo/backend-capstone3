const Product = require("../models/Product");
const User = require("../models/User")



// Create Product (Admin Only)
module.exports.createProduct = (data) =>{
	if(data.isAdmin) {
		let newProduct = new Product({
			name: data.product.name,
			description: data.product.description,
			price: data.product.price,
			isActive: data.product.isActive
		})
		
		return newProduct.save().then((product,err) => {
			if(err) {
				return false;
			} else {
				return newProduct;
			}
		})
	} else {
		return false;
	}
};

// Retrieve all active products
module.exports.getActiveProducts = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	})
}

// Retrieve all Products
module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	})
}


// Retrieving single product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	})
}


// Updating a product
module.exports.updateProduct = (reqParams,reqBody) =>{
	let updatedProduct ={
		name: reqBody.name,
		description: reqBody.description,
		price:reqBody.price
	};
	return Product.findByIdAndUpdate(reqParams.productId,updatedProduct)
		.then((product,error)=>{
			if(error){
				return false;
			}else{
				return updatedProduct;
			};
		});
};

// Archiving a product
module.exports.archiveProduct = (reqParams) => {

	console.log(reqParams);

	let updateActiveField = {
		isActive : false
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {

		if (error) {

			return false;

		} else {

			return true;

		}

	});
};


// Activating a Product
module.exports.activateProduct = (reqParams) => {

	console.log(reqParams);

	let updateActiveField = {
		isActive : true
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {

		if (error) {

			return false;

		} else {

			return true;

		}

	});
};

