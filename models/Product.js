const mongoose = require("mongoose")

const productSchema = new mongoose.Schema({
	name: String,
	description: String,
	price: Number,
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	orders: [
		
		{	
			orderId: String,
			userId: String,
			quantity: Number,
			purchasedOn:{ 
				type: Date,
				default: new Date()
			}
		}

	]
})

 module.exports = mongoose.model("Product", productSchema);
