const mongoose = require("mongoose")


const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First Name is required"]
	},
	lastName: {
		type: String,
		required: [true, "Last Name is required"]
	},
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile Number is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	orders: [
		
		{
			totalAmount: Number,
			purchasedOn: {
					type: Date,
					default: new Date()
			},
			products: [

				{
					productId: String,
					quantity: Number
				}

			]
		}

	]
})

module.exports = mongoose.model("User", userSchema);
